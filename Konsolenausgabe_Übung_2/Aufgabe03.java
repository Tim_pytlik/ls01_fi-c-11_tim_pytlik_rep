
public class Aufgabe03 {

	public static void main(String[] args) {
		System.out.printf( "%-1s", "Fahrenheit" );
			System.out.printf( "%2s", "|" );
				System.out.printf( "%10s,\n", "Celsius" );
	int Z1 = -20;
	double Z2 = -28.8889;
		System.out.printf( "%-5d" , Z1);
			System.out.printf( "%7s", "|" );
				System.out.printf("%.2f\n" , Z2);
	int Z3 = -10;
	double Z4 = -23.3333;
		System.out.printf( "%-5d", Z3);
			System.out.printf("%7s", "|");
				System.out.printf("%.2f\n" , Z4);
	int Z5 = 0;
	double Z6 = -17.7778;
		System.out.printf( "%-5d", Z5);
			System.out.printf("%7s", "|");
				System.out.printf("%.2f\n" , Z6);
	int Z7 = 20;
	double Z8 = -6.6667;
		System.out.printf( "%+-5d", Z7);
			System.out.printf("%7s", "|");
				System.out.printf("%.2f\n" , Z8);
	int Z9 = +30;
	double Z10 = -1.1111;
		System.out.printf( "%+-5d", Z9);
			System.out.printf("%7s", "|");
				System.out.printf("%.2f%6d", Z10, Z10); 
				// Nach 2 Stunden aufgegeben, weil es nicht geklappt hat mit der Formatierungsregel %d nach rechts zu formatieren. 
				}
}
